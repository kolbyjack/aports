# Contributor: Lucas Ramage <ramage.lucas@protonmail.com>
# Maintainer: Lucas Ramage <ramage.lucas@protonmail.com>
pkgname=libfyaml
pkgver=0.7.11
pkgrel=0
pkgdesc="Fully feature complete YAML parser and emitter"
url="https://github.com/pantoniou/libfyaml"
arch="all"
license="MIT"
depends="libltdl"
checkdepends="check git"
makedepends="autoconf automake bash libtool"
subpackages="$pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/pantoniou/libfyaml/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # tests failing, (See: https://github.com/pantoniou/libfyaml/issues/20)

prepare() {
	default_prepare
	./bootstrap.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/usr/lib \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

check() {
	make check
}

sha512sums="
459b2fa0f568be6f938992f8845a96e119a359a0998440d1cf3d30ac1a9f385bbf757afd1943b9686b53f105110e990ac174ac93931e3303814c9ab985b9da8e  libfyaml-0.7.11.tar.gz
"
