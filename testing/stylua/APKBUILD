# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=stylua
pkgver=0.12.1
pkgrel=0
pkgdesc="Opinionated Lua 5.1/5.2/luau code formatter"
url="https://github.com/JohnnyMorganz/StyLua"
arch="all !s390x !riscv64" # blocked by cargo
license="MPL-2.0"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://github.com/JohnnyMorganz/StyLua/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/StyLua-$pkgver"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cat >> Cargo.toml <<- EOF
		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "s"
		panic = "abort"
	EOF
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 "$builddir"/target/release/stylua \
		-t "$pkgdir"/usr/bin
}
sha512sums="
0c2bc6ca1b353c27f64c38ef62b2172349cd5dc6c27ee4da08783056ac5711179bb08e9dd77aa042c3318ec4b92ad9d67e38db630b4589405883128d2a2e599b  stylua-0.12.1.tar.gz
"
